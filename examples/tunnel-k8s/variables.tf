# Kubernetes variables
variable "config_path" {
  description = "A path to a kube config file. Can be sourced from KUBE_CONFIG_PATH"
  type        = string
  default     = "~/.kube/config"
}

variable "config_context" {
  description = "Context to choose from the config file. Can be sourced from KUBE_CTX"
  type        = string
  default     = "default"
}

# Cloudflare Variables
variable "cf_api_token" {
  description = "Cloudflare API Token with Zone permissions. See - https://cert-manager.io/docs/configuration/acme/dns01/cloudflare/"
  type        = string
  sensitive   = true
}

variable "cf_account_id" {
  description = "The Cloudflare account ID that you wish to manage the Argo Tunnel on"
  type        = string
}

variable "cf_zone_name" {
  description = "The name of the zone"
  type        = string
}

variable "tunnel_routes" {
  description = "Service Routes to create, maps DNS hostname to internal K8s service"
  type        = map(string)
  default = {
    tunnel = "http://web-service:80"
    hello  = "hello_world"
  }
}
