output "cf_zone_id" {
  description = "Cloudflare zone ID"
  value       = data.cloudflare_zone.zone.zone_id
}

output "cf_tunnel_id" {
  description = "The Argo Tunnel UUID"
  value       = cloudflare_tunnel.tunnel.id
}

output "cf_tunnel_cname" {
  description = "Usable CNAME for accessing the Argo Tunnel"
  value       = cloudflare_tunnel.tunnel.cname
}
