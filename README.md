# terraform-cloudflare-zerotrust

This repo contains modules for deploying Cloudflare resources.

<!-- UPDATE THIS SECTION WHEN ADDING NEW MODULES -->
* [tunnel-k8s](/modules/tunnel-k8s): Used to expose a kubernets application with Cloudflared and Cloudflare Access.
* [tunnel-k8s-helm](/modules/tunnel-k8s-helm): Expose a kubernetes cluster with Cloudflare Tunnel Helm Chart. See - [argo-tunnel-examples](https://github.com/cloudflare/argo-tunnel-examples/tree/master/helm/cloudflare-tunnel)

Click on each module above to see its documentation. Head over to the [examples folder](/examples) for examples.

## TODO
- Expose more configuration options
